class User < ActiveRecord::Base
  before_create :set_api_attributes

  def set_api_attributes
    self.api_key    = SecureRandom.hex(16)
  end

end