# README:
---
This README would normally document whatever steps are necessary to get the
application up and running.
---
# Goal:
---
Ensure that you, the developer has a great grasp on how RoR works and how quickly you can implement code. 

Total Time: 4-6 hours

# Problem:
---
A new company, called “scatchbling” (SB) was formed.  SB would like to have customers and other clients access an API to list all of the back scratchers that they are selling on the market.   In order to complete this, you, the developer, must create a simple RESTful interface that will provide access to the company’s database. You also need to enable user level security on the API access.

# Deliverables:
---
Working API - Should send link to a proper RESTful API. API should include:
* Read
* Write
* Create
* Update

Bundled Zip of all source code.  

Postman - Export file of all the API tests


# Technical Details:
---
Mongo Command Line: 
mongo ds033489.mongolab.com:33489/scratchbling -u django -p django123
mongodb://django:django123@ds033489.mongolab.com:33489/scratchbling

# Valid URLS
---
http://localhost:3000/api/scratchers/12?api_key=f5b1be3ed49c315e2e556447cdc5a638
http://localhost:3000/api/scratchers?api_key=f5b1be3ed49c315e2e556447cdc5a638

