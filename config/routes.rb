Rails.application.routes.draw do

  namespace :api do
    resources :scratchers
    post 'scratchers/:id/edit' => 'scratchers#edit'
  end
end

