require 'lib/encryptation_helper'

class UsersController < ApplicationController
  include EncryptationHelper

  before_filter :generate_api_key, only: [:create]

  def generate_api_key
    encrypt('challenger')
  end
end